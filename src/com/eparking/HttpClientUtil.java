package com.eparking;

import java.util.HashMap;
import java.util.Map;

import javax.xml.ws.spi.http.HttpContext;

import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSON;

public class HttpClientUtil {
	
	
	public static String postJson (String url,String paramStr) {
	   
	    int TimeOutTime = 200000;
	    long time = System.currentTimeMillis();
	    CloseableHttpResponse httpResponse = null;
	    CloseableHttpClient httpClient = HttpClients.createDefault();
	    BasicHttpContext httpContext = new BasicHttpContext();
	    try {
	        HttpPost post = new HttpPost(url);
	        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(TimeOutTime)
	                .setConnectTimeout(TimeOutTime).build();// 设置请求和传输超时时间
	        post.setHeader(new BasicHeader("Content-Type", "application/json;charset=UTF-8"));
	        HttpEntity entity = new StringEntity(paramStr,"utf-8");
	        post.setEntity(entity);
	        post.setConfig(requestConfig);
	        
	        httpResponse = httpClient.execute(post, httpContext);
	        HttpEntity entity2 = httpResponse.getEntity();
	        if (null != entity) {
	            String content = new String(EntityUtils.toString(entity2));
	            System.out.println(content);
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    } finally {
	        try {
	            httpResponse.close();
	            if (httpClient != null) {
	                httpClient.close();
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
		return null;
	}


}
