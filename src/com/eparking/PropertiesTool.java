package com.eparking;

import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;


public class PropertiesTool {
	
	/**
	 * 由key取某个属性值
	 * @param filePath
	 * @param key
	 * @return
	 */
	public static String GetValueByKey(String filePath, String key) {
	      Properties pps = new Properties();
	      try {
	          InputStream in = new BufferedInputStream (new FileInputStream(filePath));  
	          pps.load(new InputStreamReader(in, "UTF-8"));
	          String value = pps.getProperty(key);
	          return value;
	      }catch (IOException e) {
	          e.printStackTrace();
	          return null;
	      }
	  }
	
	/**
	 * 由keyLIST取多个属性值
	 * @param filePath
	 * @param keyList
	 * @return
	 */
	public static List<String> GetValueByKey(String filePath, List<String> keyList) {
	      Properties pps = new Properties();
	      List<String> valueList = new ArrayList<String>();
	      try {
	          InputStream in = new BufferedInputStream (new FileInputStream(filePath));  
	          pps.load(new InputStreamReader(in, "UTF-8"));
	          for(String key : keyList){
	        	  String value = pps.getProperty(key);
	        	  valueList.add(value);
	          }
	          return valueList;
	      }catch (IOException e) {
	          e.printStackTrace();
	          return null;
	      }
	  }
	  
	  /**
	   * 获取所有属性
	   * @param filePath
	   * @return
	   * @throws IOException
	   */
	  public static Map<String,String> GetAllProperties(String filePath) throws IOException {
	      Properties pps = new Properties();
	      Map<String,String> map = new HashMap<String,String>();
	      InputStream in = new BufferedInputStream(new FileInputStream(filePath));
	      pps.load(new InputStreamReader(in, "UTF-8"));
	      Enumeration<?> en = pps.propertyNames(); 
	      while(en.hasMoreElements()) {
	          String strKey = (String) en.nextElement();
	          String strValue = pps.getProperty(strKey);
	          map.put(strKey, strValue);
	      }
	      return map;
	      
	  }
	  
	  /**
	   * 写属性
	   * @param filePath
	   * @param map
	   * @return
	   */
	  public static int WriteProperties (String filePath, Map<String,String> map) {
		  Properties pps = new Properties();
		  try {
		      Map<String,String> allValue = GetAllProperties(filePath);
		      OutputStream out = new FileOutputStream(filePath);
		      
		      Set<Entry<String, String>> entry = map.entrySet();
		      Iterator<Entry<String, String>>  it = entry.iterator();
		      while (it.hasNext()){
		    	  Entry<String, String> ety = it.next();
		    	  pps.setProperty(ety.getKey(), ety.getValue());
		    	  allValue.remove(ety.getKey());  
		      }
		      Set<Entry<String, String>> allentry = allValue.entrySet(); 
		      Iterator<Entry<String, String>>  allit = allentry.iterator();
		      while(allit.hasNext()){  
		    	  Entry<String, String> other = allit.next();
		    	  pps.setProperty(other.getKey(), other.getValue());
		      }
	      
		      pps.store(new OutputStreamWriter(out, "UTF-8"), "");
		      return 1;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	  }
	  
	
	  
	  public static void main(String [] args) throws IOException{
		  String filePath = "D:\\wlgl-workspace\\eparking\\camera.properties";
//		  filePath = System.getProperty("user.dir")+ "\\jdbc.properties";
		  Map<String,String> map = GetAllProperties(filePath);
		  Set<String> set = map.keySet();
		  for(String k :set){
			  System.out.println("key="+k);
			  System.out.println("value="+map.get(k));
		  }

		  System.out.println("==================================");
		  
		  System.out.println(GetValueByKey(filePath,"in.DeviceIP"));
		  System.out.println(GetValueByKey(filePath,"in.UserName"));
		  System.out.println(GetValueByKey(filePath,"in.Password"));
		  System.out.println(GetValueByKey(filePath,"sendUrl"));
		  
		
	  }
	  
}
